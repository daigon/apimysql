const express = require('express');
const userRoutes = require('./routes/userRoutes');
const ideaRoutes =require('./routes/ideaRoutes');
const wendyConfigurationRoutes= require('./routes/wendyConfigurationRoutes');
const config = require('./config');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
var cors = require('cors');
var path = require('path');
const http = require('http');
var uuid = require('uuid');
const fs = require('fs');
var timeout = require('connect-timeout'); //express v4
var logger = require('morgan');

const app = express();

app.use(cors())

mongoose.Promise = global.Promise;

app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));
app.use(logger('dev')); //replaces your app.use(express.logger());


var connection = require('./services/DbConnection');

connection
.authenticate()
.then(() => {
  const User = connection.import('./models/user');
  const Idea =connection.import('./models/idea');
  const collaborator = connection.import('./models/collaborators');
  const Image = connection.import('./models/image');
  const wendyConfiguration = connection.import('./models/wendyConfiguration');
  const Comment = connection.import('./models/comment');
  const notify = connection.import('./models/notify');
  const likes = connection.import('./models/like');
  const review = connection.import('./models/review');
  console.log('Connected to mysql');
})
.catch(err => {
  console.error('Unable to connect to the database:', err);
});

  var contentType = require('content-type')
  var getRawBody = require('raw-body')
   
  app.use(function (req, res, next) {
    if (req.headers['content-type'] === 'application/octet-stream') {
     
    getRawBody(req, {
      length: req.headers['content-length'],
      limit: '20mb',
      encoding: contentType.parse(req).parameters.charset
    }, function (err, string) {
      if (err) return res.status(500).send({message:'Error al subir imagen', error:err})
      
      req.text = string
     
      next()
    })
  }
  else{
    next()
  }
  })

  
userRoutes(app);
ideaRoutes(app);
wendyConfigurationRoutes(app);




//err: population prev
//req and res: entrada y salida
//next function: pasar al siguiente middleware
app.use((err, req, res, next) => {
  res.header('Access-Control-Allow-Origin','*');
  res.header('Access-Control-Allow-Headers', 'Autorization, X-API-KEY,Origin, X-Requested_With,Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET;POST,OPTIONS,PUT,DELETE');
  res.header('Allow', 'GET;POST,OPTIONS,PUT,DELETE');
  next();
});

app.use((err, req, res, next) => {
  
  res.status(500).send({error: err.message});
});



//app.use(express.static(__dirname)); // Current directory is root
app.use(express.static(__dirname + '/dist'));

app.get('/', function(req, res) {  
    
  res.set('Content-Type', 'text/html')    
     .sendFile(path.join(__dirname, './dist/index.html'));    
});

app.get('/manageUsers', function(req, res) {    
  res.set('Content-Type', 'text/html')    
     .sendFile(path.join(__dirname, './dist/index.html'));    
});

app.get('/login', function(req, res) {    
  res.set('Content-Type', 'text/html')    
     .sendFile(path.join(__dirname, './dist/index.html'));    
});

app.get('/showIdeas', function(req, res) {    
  res.set('Content-Type', 'text/html')    
     .sendFile(path.join(__dirname, './dist/index.html'));    
});
app.get('/api/verify/:token', function(req, res) {    
  res.set('Content-Type', 'text/html')    
     .sendFile(path.join(__dirname, './dist/index.html'));    
});

// app.use(timeout(120000));
// app.use(haltOnTimedout);

function haltOnTimedout(req, res, next){
  if (!req.timedout) next();
}

module.exports = app;