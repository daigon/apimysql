const connection = require('../services/DbConnection');
const Idea = connection.import('../models/idea');
const comment = connection.import('../models/comment');
const Collaborator = connection.import('../models/collaborators');
const image = connection.import('../models/image');
const User = connection.import('../models/user');
const notify = connection.import('../models/notify');
const review = connection.import('../models/review');
const like = connection.import('../models/like');

const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const fs = require('fs');
const path = require('path');
const mongoosePaginate = require('mongoose-pagination');


const populateQuery = [
    { model: User, as: 'author' },
    { model: User, as: 'coach' },
    {
        model: Collaborator, as: 'collaborator',
        include: [{ model: User, as: 'info' }]
    },
    { model: notify, as: 'notify' },
    {
        model: image, as: 'image',
        include: [{ model: User, as: 'user' }]
    },
    {
        model: comment, as: 'comments',
        include: [{ model: User, as: 'user' }]
    },
    { model: review, as: 'review' },
    { model: like }
];
module.exports = {
    updateLikes(req, res, next) {

        try {
            var _id = req.params.id;
            var user = req.body._id;

            like.create({ user, ideaId: _id })
                .then(idea => {
                    return Idea.findById(_id, { include: populateQuery })
                })
                .then(idea => {
                    reshelperr(200, idea, res);
                })
                .catch((err) => reshelperr(500, err, res));
        } catch (error) {
            consolelog(error)
        }


    },

    removeLikes(req, res, next) {
        try {
            var _id = req.params.id;
            var id = req.body._id;


            like.destroy({
                where: {
                    [Op.and]: [{
                        user: id
                    },
                    {
                        ideaId: _id
                    }
                    ]

                }
            })
                .then(() => {
                    return Idea.findById(_id, { include: populateQuery })
                })
                .then(idea => {
                    reshelperr(200, idea, res);
                })
                .catch((err) => reshelperr(500, err, res));


        } catch (error) {
            console.log(error)
        }


    },

    create(req, res, next) {
        try {
            var _id = req.params.id;
            var commentProps = req.body;


            const commentBody = {};
            commentBody.content = commentProps.content;
            commentBody.date = commentProps.date;
            commentBody.userId = commentProps.user;
            commentBody.ideaId = _id;
            var commentModel = null;


            comment.create(commentBody)
                .then(() => {

                    return Idea.findById(_id, { include: populateQuery })
                })
                .then(idea => {

                    reshelpobject(200, idea, res);
                })
                .catch((err) => {
                    console.log(err.message)
                    reshelperr(500, err.message, res)
                });
        } catch (error) {
            console.log(error)
        }



    },

    createReview(req, res) {
        try {
            const _id = req.params.id;

            const rev = {};
            var idReview;
            rev.teamFeel = req.body.teamFeel;
            rev.safety = req.body.safety;
            rev.ergonomics = req.body.ergonomics;
            rev.customer = req.body.customer;
            rev.user = req.body.user;

            review.create(rev)
                .then((rev) => {
                    idReview = rev._id;
                    return Idea.update({ reviewid: idReview }, { where: { _id } })
                })
                .then((idea) => {

                    return Idea.findById(_id, { include: populateQuery })

                })
                .then(idea => {
                    updateNotify(idea);
                    return reshelpobject(200, idea, res);
                })
                .catch((err) => {

                    console.log(err.message)
                    return reshelperr(500, err.message, res)
                });
        } catch (error) {
            console.log(error)
        }
    },

    updateNotify(req, res, next) {

        try {

            var _id = req.params.id;
            var user = req.body.user;
            var id = req.body._id;
            notify.update({ notify: false }, {
                where: {
                    [Op.and]: [{
                        user: user
                    },
                    {
                        ideaId: _id
                    }
                    ]
                }
            })
            .then(() => {
                return Idea.findById(_id, { include: populateQuery })
            })
            .then(idea => {
                if (idea.notify.notify === false)
                    console.log("echo");
            })
            .catch(err => console.log(err.message));
            // Idea.findById({ _id })
            //     .then(idea => {
            //         for (let i = 0; i < idea.notify.length; i++) {
            //             if (idea.notify[i].id === id) {
            //                 idea.notify[i].notify = false;
            //                 return Idea.findByIdAndUpdate({ _id }, idea)
            //             }
            //         }
            //     })
            //     .then(idea => {
            //         if (idea.notify.notify === false)
            //             console.log("echo");
            //     })
            //     .catch(err => console.log(err.message));


        } catch (error) {
            console.log(error)
        }


    }

};

function updateNotify(idea) {

    try {
        for (let i = 0; i < idea.notify.length; i++) {
            idea.notify[i].notify = true;
            idea.save()
                .then(object => {

                })
                .catch(err => {
                    console.log("error => " + err.message)

                });
        }

    }
    catch (e) {
        console.log("error => " + e);
    }
}

function reshelperr(status, message, res) {
    res.status(status).send({ message: message });
}

function reshelpobject(status, comment, res) {
    res.status(status).send(comment);
}