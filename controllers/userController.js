const connection = require('../services/DbConnection');
const User = connection.import('../models/user');
const Idea = connection.import('../models/idea');
const Image = connection.import('../models/image');

const bcrypt = require('bcrypt-nodejs');
const jwt = require('../services/jwt');
const fs = require('fs');
const path = require('path');
var async = require('async-waterfall');
var crypt = require("crypto");
var nodemailer = require('nodemailer');

const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const smtpTrans = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'jonathanmedina1809@gmail.com',
        pass: 'HaloReach1809.'
    }
});

module.exports = {
    //falta validación
    create(req, res, next) {
        const userProps = req.body;
        userProps.fullname.toUpperCase();
        userProps.name.toUpperCase();
        userProps.surname.toUpperCase();

        userProps.idEmployee.toLowerCase();

        bcrypt.hash(userProps.password, null, null, (err, hash) => {
            userProps.password = hash;

            User.create(userProps)
                .then(user => reshelpobject(200, user, res))
                .catch(error => {
                    reshelperr(500, error.message, res)
                });
        });
    },
    getUser(req, res) {
        const userId = req.params.id;

        User.findById(userId)
            .then(user => {

                return res.status('200').send(user)
            })
            .catch(error => {
                reshelperr(500, error.message, res)
            });
    },

    getUsers(req, res) {


        var page = req.params.page;
        if (!page) {
            page = 1;
        }

        var limit = 10;   // number of records per page
        var offset = 0;



        User.findAndCountAll()
            .then(result => {
                pages = Math.ceil(result.count / limit);
                offset = limit * (page - 1);
                return User.findAll({
                    limit: limit,
                    offset: offset,
                    $sort: { id: 1 }
                })

            })
            .then(result => {

                return reshelpobject(200, { users: result, pages }, res);
            })
            .catch(err => {
                console.log(err);

                return reshelperr(500, err.original.sqlMessage, res);
            });

    },
    getAllUsers(req, res, next) {

        try {
            User.findAll({})
                .then(result => {

                    return reshelpobject(200, { users: result }, res);
                })
                .catch(err => {
                    console.log(err);

                    return reshelperr(500, err.original.sqlMessage, res);
                });
        } catch (error) {
            console.log(error);
            return reshelperr(500, 'Server internal error', res);
        }

    },

    getUsersBySearch(req, res, next) {
        var criteria = req.body.criteria.toUpperCase();
        var page = req.params.page;
        if (!page) {
            page = 1;
        }
        var limit = 10;   // number of records per page
        var offset = 0;

        User.findAndCountAll({
            where: {
                [Op.or]: [{
                    fullname: {
                        [Op.like]: '%' + criteria + '%'
                    }
                },
                {
                    idEmployee: {
                        [Op.like]: '%' + criteria + '%'
                    }
                }
                ]

            }
        })
            .then(result => {
                console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **********************************************')
                pages = Math.ceil(result.count / limit);
                offset = limit * (page - 1);
                return User.findAll({
                    limit: limit,
                    offset: offset,
                    $sort: { name: 1 },
                    where: {
                        [Op.or]: [{
                            fullname: {
                                [Op.like]: '%' + criteria + '%'
                            }
                        },
                        {
                            idEmployee: {
                                [Op.like]: '%' + criteria + '%'
                            }
                        }
                        ]

                    }
                })

            })
            .then(result => {

                return reshelpobject(200, { users: result, pages }, res);
            })
            .catch(err => {
                console.log(err);

                return reshelperr(500, err.original.sqlMessage, res);
            });

    },

    getUsersBySearchAndRole(req, res, next) {
        var criteria = req.body.criteria.toUpperCase();
        var page = req.params.page;
        var role = req.body.role;
        if (!page) {
            page = 1;
        }
        var limit = 10;   // number of records per page
        var offset = 0;
        var query = {
            [Op.and]: [{
                [Op.or]: [{ fullname: { [Op.like]: '%' + criteria + '%' } }, {
                    idEmployee: {
                        [Op.like]: '%' + criteria + '%'
                    }
                }
                ]
            },
            {
                ideaRole: role
            }
            ]
        }
        User.findAndCountAll({
            where: query
        })
            .then(result => {
                pages = Math.ceil(result.count / limit);
                offset = limit * (page - 1);
                return User.findAll({
                    limit: limit,
                    offset: offset,
                    $sort: { id: 1 },
                    where: query
                })

            })
            .then(result => {

                return reshelpobject(200, { users: result, pages }, res);
            })
            .catch(err => {
                console.log(err);

                return reshelperr(500, err.original.sqlMessage, res);
            });

    },

    getCoachs(req, res, next) {

        var page = req.params.page;
        if (!page) {
            page = 1;
        }
        var limit = 10;   // number of records per page
        var offset = 0;
        User.findAndCountAll({
            where: {
                [Op.or]: [{
                    ideaRole: 'COACH'
                },
                {
                    ideaRole: 'BOTH'
                }
                ]
            }
        })
            .then(result => {
                pages = Math.ceil(result.count / limit);
                offset = limit * (page - 1);
                return User.findAll({
                    limit: limit,
                    offset: offset,
                    $sort: { name: 1 },
                    where: {
                        [Op.or]: [{
                            ideaRole: 'COACH'
                        },
                        {
                            ideaRole: 'BOTH'
                        }
                        ]
                    }
                })

            })
            .then(result => {

                return reshelpobject(200, { coaches: result, pages }, res);
            })
            .catch(err => {
                console.log(err);

                return reshelperr(500, err.original.sqlMessage, res);
            });
    },

    getCollaborators(req, res, next) {
        var page = req.params.page;
        if (!page) {
            page = 1;
        }
        var limit = 10;   // number of records per page
        var offset = 0;
        User.findAndCountAll({
            where: {
                [Op.or]: [{
                    ideaRole: 'COLLABORATOR'
                },
                {
                    ideaRole: 'BOTH'
                }
                ]
            }
        })
            .then(result => {
                pages = Math.ceil(result.count / limit);
                offset = limit * (page - 1);
                return User.findAll({
                    limit: limit,
                    offset: offset,
                    $sort: { name: 1 },
                    where: {
                        [Op.or]: [{
                            ideaRole: 'COLLABORATOR'
                        },
                        {
                            ideaRole: 'BOTH'
                        }
                        ]
                    }
                })

            })
            .then(result => {

                return reshelpobject(200, { collaborators: result, pages }, res);
            })
            .catch(err => {
                console.log(err);

                return reshelperr(500, err.original.sqlMessage, res);
            });
    },
    //falta validación
    loginWeb(req, res, next) {

        const userProps = req.body;

        User.findOne({ where: { idEmployee: userProps.idEmployee.toLowerCase() } })
            .then(user => {

                if (user != null) {

                    bcrypt.compare(userProps.password, user.password, (err, check) => {
                        if (err) {
                            return reshelperr(500, 'Error' + err, res);
                        }

                        if (check) {
                            //devolver token de JWT
                            if (userProps.getHash)
                                reshelpobject(200, {
                                    token: jwt(user)
                                }, res);
                            else
                                reshelpobject(200, user, res);
                            // 5

                        }
                        else {
                            reshelperr(403, 'La contraseña no coincide', res);
                        }
                    });

                } else
                    reshelperr(400, 'No existe el usuario', res);

            })
            .catch(error => {
                reshelperr(500, error.message, res)
            });

    },

    verifyIdUser(req, res, next) {
        try {
            const userProps = req.body;
            if (userProps.idEmployee !== undefined) {
                User.findOne({
                    where: {
                        idEmployee: userProps.idEmployee.toLowerCase()
                    }
                })
                    .then(user => {

                        if (user != null) {
                            reshelperr(200, { id: 'El empleado ' + user.fullname + ' ya tiene ese id', value: 1 }, res);
                        } else
                            reshelperr(200, { id: 'Id disponible', value: 0 }, res);

                    })
                    .catch(error => {
                        reshelperr(500, error.message, res)
                    });
            }
        }
        catch (e) {
            console.log(e);
        }

    },

    verifyEmail(req, res, next) {
        try {
            const userProps = req.body;
            if (userProps.email !== undefined) {
                User.findOne({
                    where: {
                        email: userProps.email.toLowerCase()
                    }
                })
                    .then(user => {

                        if (user != null) {
                            reshelperr(200, { email: 'El empleado ' + user.fullname + ' ya tiene ese email', value: 1 }, res);
                        } else
                            reshelperr(200, { email: 'Email disponible', value: 0 }, res);

                    })
                    .catch(error => {
                        reshelperr(500, error.message, res)
                    });
            }
        }
        catch (e) {
            console.log(e);
        }

    },

    loginMovil(req, res, next) {

        const userProps = req.body;

        User.findOne({
            where: {
                idEmployee: userProps.idEmployee.toLowerCase()
            }
        })
            .then(user => {
                if (user != null) {
                    if (userProps.getHash)
                        reshelpobject(200, {
                            token: jwt(user)
                        }, res);
                    else
                        reshelpobject(200, user, res);
                } else
                    reshelperr(400, 'No existe el usuario', res);

            })
            .catch(error => {
                reshelperr(500, error.message, res)
            });

    },

    delete(req, res, next) {
        const userId = req.params.id;
        try {
            User.destroy({
                where: {
                    _id: userId
                }
            })
                .then(user => {

                    reshelperr(200, 'El usario a sido borrado', res);

                })
                .catch(error => {
                    reshelperr(500, error.message, res);
                });
        }
        catch (e) {
            reshelperr(500, 'Error en el servidor')
        }

    },
    //cambiar contraseña
    update(req, res, next) {
        const userId = req.params.id;
        const userProps = req.body;
        if (userProps.password !== undefined) {

            bcrypt.hash(userProps.password, null, null, (err, hash) => {
                userProps.password = hash;

                User.update(userProps, {
                    where: {
                        _id: userId
                    }
                })
                    .then(() => User.findById(userId))
                    .then(user => {

                        res.status('200').send(user)
                    })
                    .catch(error => {
                        reshelperr(500, error.message, res)
                    });
            });
        }
        else {

            User.update(userProps, {
                where: {
                    _id: userId
                }
            })
                .then(() => User.findById(userId))
                .then(user => {

                    res.status('200').send(user)
                })
                .catch(error => {
                    reshelperr(500, error.message, res)
                });
        }





    },

    uploadImageWeb(req, res, next) {
        const _id = req.params.id;
        var file_name = 'ideas.png';
        const file_path = req.files.image.path;
        if (req.files) {



            file_name = path.basename(file_path);

            var ext_split = file_name.split('\.');
            var file_ext = ext_split[1];

            if (file_ext == 'png' || file_ext == 'jpg' || file_ext == 'gif') {


                User.update({ image: file_name }, { where: { _id: _id } })
                    .then(() => User.findById(_id))
                    .then(user => {

                        if (user !== null) {
                            res.status('200').send({
                                user,
                                image: file_name
                            });
                            if (req.headers.oldimage !== 'userDefault.png') {
                                const rootPath = path.normalize(__dirname + '/../../uploads/users');
                                const oldImage = rootPath + '\\' + req.headers.oldimage;
                                fs.unlink(oldImage, (err => {
                                    if (err) {
                                        console.log(err)
                                    }
                                }));
                            }
                            return;
                        }
                        else
                            reshelperr(400, 'no se encuentra el usuario')

                    })
                    .catch(error => {
                        return reshelperr(500, error.message, res)
                    });
            } else {
                res.status(200).send({
                    message: 'Extension del archivo no valida'
                });
                fs.unlink(file_path, (err => {
                    if (err) {
                        console.log(err)
                    }
                }));
                return;
            }

        } else {
            res.status(400).send({
                message: 'No has subido una imagen ...'
            });
        }
    },

    getImageFile(req, res, next) {
        const imageFile = req.params.imageFile;
        const pathfile = './uploads/users/' + imageFile;

        fs.exists(pathfile, (exists) => {
            if (exists)
                res.sendFile(path.resolve(pathfile));
            else
                res.status(200).send({
                    message: 'No has subido una imagen ...'
                });
        });


    },
    // ********************************************************** Forgot Password *********************************************

    //*********** enviar correo ***********
    forgotPassword(req, res, next) {


        try {

            async([
                function (done) {
                    crypt.randomBytes(20, function (err, buf) {
                        var token = buf.toString('hex');
                        console.log('done 1');
                        done(err, token);
                    });
                },
                function (token, done) {

                    User.findOne({ where: { email: req.body.email } }, function (err, user) {
                        if (!user) {
                            //   console.log(500, 'No account with that email address exists.');
                            return reshelperr(500, 'El email ' + req.body.email + ' no esta registrado en el sistema.', res);

                        }

                        user.resetPasswordToken = token;
                        user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

                        user.save(function (err) {
                            done(err, token, user);
                        });


                    });
                },
                function (token, user, done) {


                    var mailOptions = {

                        to: user.email,
                        from: 'jonathanmedina1809@gmail.com',
                        subject: 'Cambiar contraseña',
                        html: getHtmlMail(user, req, token)

                    };


                    smtpTrans.sendMail(mailOptions, function (err) {
                        if (err) {
                            console.log(err);
                            return reshelperr(500, err);

                        }
                        return reshelperr(200, "Se ha enviado un correo con las instrucciones para cambiar su contraseña", res);


                    });
                }
            ], function (err) {
                console.log('this err' + ' ' + err);
                return reshelperr(500, err)

            });
        } catch (e) {
            console.log(e);
        }
    },
    //*********** fin  enviar correo ***********
    verifyToken(req, res) {
        User.findOne({
            where: {
                [Op.and]: [{
                    resetPasswordToken: req.params.token
                },
                {
                    resetPasswordExpires: { $gt: Date.now() }
                }
                ]
            }
        },
            function (err, user) {

                if (!user) {
                    res.set('Content-Type', 'text/html')
                        .sendFile(path.join(__dirname, '../dist/index.html'));
                }
                res.set('Content-Type', 'text/html')
                    .sendFile(path.join(__dirname, '../dist/index.html'));
            });
    },

    resetPassword(req, res) {

        try {
            async([
                function (done) {

                    User.findOne({
                        where: {
                            [Op.and]: [{
                                resetPasswordToken: req.params.token
                            },
                            {
                                resetPasswordExpires: { $gt: Date.now() }
                            }
                            ]
                        }
                    },
                        function (err, user, next) {
                            if (!user) {
                                return reshelperr(500, 'El correo ha expirado', res);

                            }

                            if (req.body.password !== undefined) {
                                bcrypt.hash(req.body.password, null, null, (err, hash) => {
                                    if (err) {
                                        console.log(err)
                                        return reshelperr(500, "No se pudo cambiar la contraseña", res);
                                    }
                                    user.password = hash;
                                    user.resetPasswordToken = undefined;
                                    user.resetPasswordExpires = undefined;

                                    user.save(function (err) {
                                        if (err) {
                                            console.log(err)
                                            return reshelperr(500, "No se pudo cambiar la contraseña", res);
                                        } else {
                                            reshelperr(200, "La contraseña ha sido cambiada", res);
                                            done(err, user);

                                        }
                                    });
                                });
                            }

                        });
                },
                function (user, done) {
                    var mailOptions = {
                        to: user.email,
                        from: 'jonathanmedina1809@htomail.com',
                        subject: 'Contraseña cambiada',
                        html: getHtmlMailCorrect(user, req)
                    };
                    smtpTrans.sendMail(mailOptions, function (err) {
                        if (err) {
                            console.log(err);
                            return reshelperr(500, err, res);

                        }
                    });
                }
            ], function (err) {
                console.log(err);
                return reshelperr(500, err);
            });
        } catch (error) {
            console.log(error)
        }

    },
};

function reshelperr(status, message, res) {
    res.status(status).send({
        message: message
    });
}

function reshelpobject(status, album, res) {
    res.status(status).send(album);
}

function getHtmlMail(user, req, token) {
    return "<!doctype html>" +
        "<html>" +
        "  <head>" +
        "    <meta name=\"viewport\" content=\"width=device-width\" />" +
        "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />" +
        "    <title>Simple Transactional Email</title>" +
        "    <style>" +
        "      /* -------------------------------------" +
        "          GLOBAL RESETS" +
        "      ------------------------------------- */" +
        "      img {" +
        "        border: none;" +
        "        -ms-interpolation-mode: bicubic;" +
        "        max-width: 100%; }" +
        "      body {" +
        "        background-color: #26344c;" +
        "        font-family: sans-serif;" +
        "        -webkit-font-smoothing: antialiased;" +
        "        font-size: 14px;" +
        "        line-height: 1.4;" +
        "        margin: 0;" +
        "        padding: 0;" +
        "        -ms-text-size-adjust: 100%;" +
        "        -webkit-text-size-adjust: 100%; }" +
        "      table {" +
        "background-color: #26344c;" +
        "        border-collapse: separate;" +
        "        mso-table-lspace: 0pt;" +
        "        mso-table-rspace: 0pt;" +
        "        width: 100%; }" +
        "        table td {" +
        "          font-family: sans-serif;" +
        "          font-size: 14px;" +
        "          vertical-align: top; }" +
        "      /* -------------------------------------" +
        "          BODY & CONTAINER" +
        "      ------------------------------------- */" +
        "      .body {" +
        "        background-color: #26344c;" +
        "        width: 100%; }" +
        "      /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */" +
        "      .container {" +
        "        display: block;" +
        "        Margin: 0 auto !important;" +
        "        /* makes it centered */" +
        "        max-width: 580px;" +
        "        padding: 10px;" +
        "        width: 580px; }" +
        "      /* This should also be a block element, so that it will fill 100% of the .container */" +
        "      .content {" +
        "        box-sizing: border-box;" +
        "        display: block;" +
        "        Margin: 0 auto;" +
        "        max-width: 580px;" +
        "        padding: 10px; }" +
        "      /* -------------------------------------" +
        "          HEADER, FOOTER, MAIN" +
        "      ------------------------------------- */" +
        "      .main {" +
        "        background: #ffffff;" +
        "        border-radius: 3px;" +
        "        width: 100%; }" +
        "      .wrapper {" +
        "        box-sizing: border-box;" +
        "        padding: 20px; }" +
        "      .content-block {" +
        "        padding-bottom: 10px;" +
        "        padding-top: 10px;" +
        "      }" +
        "      .footer {" +
        "        clear: both;" +
        "        Margin-top: 10px;" +
        "        text-align: center;" +
        "        width: 100%; }" +
        "        .footer td," +
        "        .footer p," +
        "        .footer span," +
        "        .footer a {" +
        "          color: #999999;" +
        "          font-size: 12px;" +
        "          text-align: center; }" +
        "      /* -------------------------------------" +
        "          TYPOGRAPHY" +
        "      ------------------------------------- */" +
        "      h1," +
        "      h2," +
        "      h3," +
        "      h4 {" +
        "        color: #000000;" +
        "        font-family: sans-serif;" +
        "        font-weight: 400;" +
        "        line-height: 1.4;" +
        "        margin: 0;" +
        "        Margin-bottom: 30px; }" +
        "      h1 {" +
        "        font-size: 35px;" +
        "        font-weight: 300;" +
        "        text-align: center;" +
        "        text-transform: capitalize; }" +
        "      p," +
        "      ul," +
        "      ol {" +
        "        font-family: sans-serif;" +
        "        font-size: 14px;" +
        "        font-weight: normal;" +
        "        margin: 0;" +
        "        Margin-bottom: 15px; }" +
        "        p li," +
        "        ul li," +
        "        ol li {" +
        "          list-style-position: inside;" +
        "          margin-left: 5px; }" +
        "      a {" +
        "        color: #3498db;" +
        "        text-decoration: underline; }" +
        "      /* -------------------------------------" +
        "          BUTTONS" +
        "      ------------------------------------- */" +
        "      .btn {" +
        "        box-sizing: border-box;" +
        "        width: 100%; }" +
        "        .btn > tbody > tr > td {" +
        "          padding-bottom: 15px; }" +
        "        .btn table {" +
        "          width: auto; }" +
        "        .btn table td {" +
        "          background-color: #26344c;" +
        "          border-radius: 5px;" +
        "          text-align: center; }" +
        "        .btn a {" +
        "          background-color: #26344c;" +
        "          border: solid 1px #3498db;" +
        "          border-radius: 5px;" +
        "          box-sizing: border-box;" +
        "          color: #3498db;" +
        "          cursor: pointer;" +
        "          display: inline-block;" +
        "          font-size: 14px;" +
        "          font-weight: bold;" +
        "          margin: 0;" +
        "          padding: 12px 25px;" +
        "          text-decoration: none;" +
        "          text-transform: capitalize; }" +
        "      .btn-primary table td {" +
        "background: #00b6d1; /* For browsers that do not support gradients */" +
        "background: -webkit-linear-gradient(left, #00b6d1 , #00beb3); /* For Safari 5.1 to 6.0 */" +
        " background: -o-linear-gradient(right, #00b6d1, #00beb3); /* For Opera 11.1 to 12.0 */" +
        "background: -moz-linear-gradient(right, #00b6d1, #00beb3); /* For Firefox 3.6 to 15 */" +
        "background: linear-gradient(to right, #00b6d1 , #00beb3); /* Standard syntax */ }" +
        "      .btn-primary a {" +
        "        background-color: #3498db;" +
        "        border-color: #3498db;" +
        "        color: #ffffff; }" +
        "      /* -------------------------------------" +
        "          OTHER STYLES THAT MIGHT BE USEFUL" +
        "      ------------------------------------- */" +
        "      .last {" +
        "        margin-bottom: 0; }" +
        "      .first {" +
        "        margin-top: 0; }" +
        "      .align-center {" +
        "        text-align: center; }" +
        "      .align-right {" +
        "        text-align: right; }" +
        "      .align-left {" +
        "        text-align: left; }" +
        "      .clear {" +
        "        clear: both; }" +
        "      .mt0 {" +
        "        margin-top: 0; }" +
        "      .mb0 {" +
        "        margin-bottom: 0; }" +
        "      .preheader {" +
        "        color: transparent;" +
        "        display: none;" +
        "        height: 0;" +
        "        max-height: 0;" +
        "        max-width: 0;" +
        "        opacity: 0;" +
        "        overflow: hidden;" +
        "        mso-hide: all;" +
        "        visibility: hidden;" +
        "        width: 0; }" +
        "      .powered-by a {" +
        "        text-decoration: none; }" +
        "      hr {" +
        "        border: 0;" +
        "        border-bottom: 1px solid #f6f6f6;" +
        "        Margin: 20px 0; }" +
        "      /* -------------------------------------" +
        "          RESPONSIVE AND MOBILE FRIENDLY STYLES" +
        "      ------------------------------------- */" +
        "      @media only screen and (max-width: 620px) {" +
        "        table[class=body] h1 {" +
        "          font-size: 28px !important;" +
        "          margin-bottom: 10px !important; }" +
        "        table[class=body] p," +
        "        table[class=body] ul," +
        "        table[class=body] ol," +
        "        table[class=body] td," +
        "        table[class=body] span," +
        "        table[class=body] a {" +
        "          font-size: 16px !important; }" +
        "        table[class=body] .wrapper," +
        "        table[class=body] .article {" +
        "          padding: 10px !important; }" +
        "        table[class=body] .content {" +
        "          padding: 0 !important; }" +
        "        table[class=body] .container {" +
        "          padding: 0 !important;" +
        "          width: 100% !important; }" +
        "        table[class=body] .main {" +
        "          border-left-width: 0 !important;" +
        "          border-radius: 0 !important;" +
        "          border-right-width: 0 !important; }" +
        "        table[class=body] .btn table {" +
        "          width: 100% !important; }" +
        "        table[class=body] .btn a {" +
        "          width: 100% !important; }" +
        "        table[class=body] .img-responsive {" +
        "          height: auto !important;" +
        "          max-width: 100% !important;" +
        "          width: auto !important; }}" +
        "      /* -------------------------------------" +
        "          PRESERVE THESE STYLES IN THE HEAD" +
        "      ------------------------------------- */" +
        "      @media all {" +
        "        .ExternalClass {" +
        "          width: 100%; }" +
        "        .ExternalClass," +
        "        .ExternalClass p," +
        "        .ExternalClass span," +
        "        .ExternalClass font," +
        "        .ExternalClass td," +
        "        .ExternalClass div {" +
        "          line-height: 100%; }" +
        "        .apple-link a {" +
        "          color: inherit !important;" +
        "          font-family: inherit !important;" +
        "          font-size: inherit !important;" +
        "          font-weight: inherit !important;" +
        "          line-height: inherit !important;" +
        "          text-decoration: none !important; }" +
        "        .btn-primary table td:hover {" +
        "background: #00b6d1; /* For browsers that do not support gradients */" +
        "background: -webkit-linear-gradient(left, #00b6d1 , #00beb3); /* For Safari 5.1 to 6.0 */" +
        " background: -o-linear-gradient(right, #00b6d1, #00beb3); /* For Opera 11.1 to 12.0 */" +
        "background: -moz-linear-gradient(right, #00b6d1, #00beb3); /* For Firefox 3.6 to 15 */" +
        "background: linear-gradient(to right, #00b6d1 , #00beb3); /* Standard syntax */ }" +
        "        .btn-primary a:hover {" +
        "         " +
        "          border-color: #34495e !important; } }" +
        "    </style>" +
        "  </head>" +
        "  <body class=\"\" style=\" background-color: #26344c !important;color: #ffffff;\">" +
        "    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"body\" style=\" background-color: #26344c !important;\">" +
        "      <tr>" +
        "        <td>&nbsp;</td>" +
        "        <td class=\"container\" style=\" background-color: #26344c !important;\">" +
        "          <div class=\"content\" style=\" background-color: #26344c !important;\">" +
        "" +
        "            <!-- START CENTERED WHITE CONTAINER -->" +
        "            <span class=\"preheader\">¿Olvidaste tu contraseña?</span>" +
        "            <table class=\"main\"style=\" background-color: #26344c !important;\">" +
        "" +
        "              <!-- START MAIN CONTENT AREA -->" +
        "              <tr>" +
        "                <td class=\"wrapper\">" +
        "                  <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"style=\" background-color: #26344c !important;\" >" +
        "                    <tr>" +
        "                      <td>" +
        "<img src=\"cid:http://" + req.headers.host + "/api/user/get-image-user/ideas.png\" alt=\"Ideas\" style=\"width:200px;height:200px;\" align:\"center\">" +
        "                        <p>¿Olvidaste tu contraseña?</p>" +
        "                        <p>Haz clic en el botón dentro de la siguiente hora para establecer una nueva contraseña para tu cuenta.</p>" +
        "                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"btn btn-primary\">" +
        "                          <tbody>" +
        "                            <tr>" +
        "                              <td align=\"left\">" +
        "                                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">" +
        "                                  <tbody>" +
        "                                    <tr>" +
        "                                      <td> <a href=\"http://" + req.headers.host + "/api/verify/" + token + "\" target=\"_blank\">Restablecer contraseña</a> </td>" +
        "                                    </tr>" +
        "                                  </tbody>" +
        "                                </table>" +
        "                              </td>" +
        "                            </tr>" +
        "                          </tbody>" +
        "                        </table>" +
        "                        <p>Este es un correo electrónico automático; si lo recibiste por error, no tienes que hacer nada.</p>" +
        "                      </td>" +
        "                    </tr>" +
        "                  </table>" +
        "                </td>" +
        "              </tr>" +
        "" +
        "            <!-- END MAIN CONTENT AREA -->" +
        "            </table>" +
        "" +
        "  </body>" +
        "</html>";
}

function getHtmlMailCorrect(user, req) {
    return "<!doctype html>" +
        "<html>" +
        "  <head>" +
        "    <meta name=\"viewport\" content=\"width=device-width\" />" +
        "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />" +
        "    <title>Simple Transactional Email</title>" +
        "    <style>" +
        "      /* -------------------------------------" +
        "          GLOBAL RESETS" +
        "      ------------------------------------- */" +
        "      img {" +
        "        border: none;" +
        "        -ms-interpolation-mode: bicubic;" +
        "        max-width: 100%; }" +
        "      body {" +
        "        background-color: #26344c;" +
        "        font-family: sans-serif;" +
        "        -webkit-font-smoothing: antialiased;" +
        "        font-size: 14px;" +
        "        line-height: 1.4;" +
        "        margin: 0;" +
        "        padding: 0;" +
        "        -ms-text-size-adjust: 100%;" +
        "        -webkit-text-size-adjust: 100%; }" +
        "      table {" +
        "background-color: #26344c;" +
        "        border-collapse: separate;" +
        "        mso-table-lspace: 0pt;" +
        "        mso-table-rspace: 0pt;" +
        "        width: 100%; }" +
        "        table td {" +
        "          font-family: sans-serif;" +
        "          font-size: 14px;" +
        "          vertical-align: top; }" +
        "      /* -------------------------------------" +
        "          BODY & CONTAINER" +
        "      ------------------------------------- */" +
        "      .body {" +
        "        background-color: #26344c;" +
        "        width: 100%; }" +
        "      /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */" +
        "      .container {" +
        "        display: block;" +
        "        Margin: 0 auto !important;" +
        "        /* makes it centered */" +
        "        max-width: 580px;" +
        "        padding: 10px;" +
        "        width: 580px; }" +
        "      /* This should also be a block element, so that it will fill 100% of the .container */" +
        "      .content {" +
        "        box-sizing: border-box;" +
        "        display: block;" +
        "        Margin: 0 auto;" +
        "        max-width: 580px;" +
        "        padding: 10px; }" +
        "      /* -------------------------------------" +
        "          HEADER, FOOTER, MAIN" +
        "      ------------------------------------- */" +
        "      .main {" +
        "        background: #ffffff;" +
        "        border-radius: 3px;" +
        "        width: 100%; }" +
        "      .wrapper {" +
        "        box-sizing: border-box;" +
        "        padding: 20px; }" +
        "      .content-block {" +
        "        padding-bottom: 10px;" +
        "        padding-top: 10px;" +
        "      }" +
        "      .footer {" +
        "        clear: both;" +
        "        Margin-top: 10px;" +
        "        text-align: center;" +
        "        width: 100%; }" +
        "        .footer td," +
        "        .footer p," +
        "        .footer span," +
        "        .footer a {" +
        "          color: #999999;" +
        "          font-size: 12px;" +
        "          text-align: center; }" +
        "      /* -------------------------------------" +
        "          TYPOGRAPHY" +
        "      ------------------------------------- */" +
        "      h1," +
        "      h2," +
        "      h3," +
        "      h4 {" +
        "        color: #ffffff;" +
        "        font-family: sans-serif;" +
        "        font-weight: 400;" +
        "        line-height: 1.4;" +
        "        margin: 0;" +
        "        Margin-bottom: 30px; }" +
        "      h1 {" +
        "        font-size: 35px;" +
        "        font-weight: 300;" +
        "        text-align: center;" +
        "        text-transform: capitalize; }" +
        "      p," +
        "      ul," +
        "      ol {" +
        "        font-family: sans-serif;" +
        "        font-size: 14px;" +
        "        font-weight: normal;" +
        "        margin: 0;" +
        "        Margin-bottom: 15px; }" +
        "        p li," +
        "        ul li," +
        "        ol li {" +
        "          list-style-position: inside;" +
        "          margin-left: 5px; }" +
        "      a {" +
        "        color: #3498db;" +
        "        text-decoration: underline; }" +
        "      /* -------------------------------------" +
        "          BUTTONS" +
        "      ------------------------------------- */" +
        "      .btn {" +
        "        box-sizing: border-box;" +
        "        width: 100%; }" +
        "        .btn > tbody > tr > td {" +
        "          padding-bottom: 15px; }" +
        "        .btn table {" +
        "          width: auto; }" +
        "        .btn table td {" +
        "          background-color: #26344c;" +
        "          border-radius: 5px;" +
        "          text-align: center; }" +
        "        .btn a {" +
        "          background-color: #26344c;" +
        "          border: solid 1px #3498db;" +
        "          border-radius: 5px;" +
        "          box-sizing: border-box;" +
        "          color: #3498db;" +
        "          cursor: pointer;" +
        "          display: inline-block;" +
        "          font-size: 14px;" +
        "          font-weight: bold;" +
        "          margin: 0;" +
        "          padding: 12px 25px;" +
        "          text-decoration: none;" +
        "          text-transform: capitalize; }" +
        "      .btn-primary table td {" +
        "        background-color: #3498db; }" +
        "      .btn-primary a {" +
        "        background-color: #3498db;" +
        "        border-color: #3498db;" +
        "        color: #ffffff; }" +
        "      /* -------------------------------------" +
        "          OTHER STYLES THAT MIGHT BE USEFUL" +
        "      ------------------------------------- */" +
        "      .last {" +
        "        margin-bottom: 0; }" +
        "      .first {" +
        "        margin-top: 0; }" +
        "      .align-center {" +
        "        text-align: center; }" +
        "      .align-right {" +
        "        text-align: right; }" +
        "      .align-left {" +
        "        text-align: left; }" +
        "      .clear {" +
        "        clear: both; }" +
        "      .mt0 {" +
        "        margin-top: 0; }" +
        "      .mb0 {" +
        "        margin-bottom: 0; }" +
        "      .preheader {" +
        "        color: transparent;" +
        "        display: none;" +
        "        height: 0;" +
        "        max-height: 0;" +
        "        max-width: 0;" +
        "        opacity: 0;" +
        "        overflow: hidden;" +
        "        mso-hide: all;" +
        "        visibility: hidden;" +
        "        width: 0; }" +
        "      .powered-by a {" +
        "        text-decoration: none; }" +
        "      hr {" +
        "        border: 0;" +
        "        border-bottom: 1px solid #f6f6f6;" +
        "        Margin: 20px 0; }" +
        "      /* -------------------------------------" +
        "          RESPONSIVE AND MOBILE FRIENDLY STYLES" +
        "      ------------------------------------- */" +
        "      @media only screen and (max-width: 620px) {" +
        "        table[class=body] h1 {" +
        "          font-size: 28px !important;" +
        "          margin-bottom: 10px !important; }" +
        "        table[class=body] p," +
        "        table[class=body] ul," +
        "        table[class=body] ol," +
        "        table[class=body] td," +
        "        table[class=body] span," +
        "        table[class=body] a {" +
        "          font-size: 16px !important; }" +
        "        table[class=body] .wrapper," +
        "        table[class=body] .article {" +
        "          padding: 10px !important; }" +
        "        table[class=body] .content {" +
        "          padding: 0 !important; }" +
        "        table[class=body] .container {" +
        "          padding: 0 !important;" +
        "          width: 100% !important; }" +
        "        table[class=body] .main {" +
        "          border-left-width: 0 !important;" +
        "          border-radius: 0 !important;" +
        "          border-right-width: 0 !important; }" +
        "        table[class=body] .btn table {" +
        "          width: 100% !important; }" +
        "        table[class=body] .btn a {" +
        "          width: 100% !important; }" +
        "        table[class=body] .img-responsive {" +
        "          height: auto !important;" +
        "          max-width: 100% !important;" +
        "          width: auto !important; }}" +
        "      /* -------------------------------------" +
        "          PRESERVE THESE STYLES IN THE HEAD" +
        "      ------------------------------------- */" +
        "      @media all {" +
        "        .ExternalClass {" +
        "          width: 100%; }" +
        "        .ExternalClass," +
        "        .ExternalClass p," +
        "        .ExternalClass span," +
        "        .ExternalClass font," +
        "        .ExternalClass td," +
        "        .ExternalClass div {" +
        "          line-height: 100%; }" +
        "        .apple-link a {" +
        "          color: inherit !important;" +
        "          font-family: inherit !important;" +
        "          font-size: inherit !important;" +
        "          font-weight: inherit !important;" +
        "          line-height: inherit !important;" +
        "          text-decoration: none !important; }" +
        "        .btn-primary table td:hover {" +
        "          background-color: #34495e !important; }" +
        "        .btn-primary a:hover {" +
        "          background-color: #34495e !important;" +
        "          border-color: #34495e !important; } }" +
        "    </style>" +
        "  </head>" +
        "  <body class=\"\" style=\" background-color: #26344c !important;color: #ffffff;\">" +
        "    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"body\" style=\" background-color: #26344c !important;\">" +
        "      <tr>" +
        "        <td>&nbsp;</td>" +
        "        <td class=\"container\" style=\" background-color: #26344c !important;\">" +
        "          <div class=\"content\" style=\" background-color: #26344c !important;\">" +
        "" +
        "            <!-- START CENTERED WHITE CONTAINER -->" +
        "            <span class=\"preheader\">Tu contraseña ha sido restablecida</span>" +
        "            <table class=\"main\"style=\" background-color: #26344c !important;\">" +
        "" +
        "              <!-- START MAIN CONTENT AREA -->" +
        "              <tr>" +
        "                <td class=\"wrapper\">" +
        "                  <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"style=\" background-color: #26344c !important;\" >" +
        "                    <tr>" +
        "                      <td>" +
        "<img src=\"http://" + req.headers.host + "/api/user/get-image-user/ideas.png\" alt=\"Ideas\" style=\"width:200px;height:200px;\" align:\"center\">" +
        "                        <h2>Tu contraseña ha sido restablecida</h2>" +
        "                        <p>Se ha confirmado que la contraseña de tu cuenta " + user.idEmployee + " ha sido cambiada correctamente.</p>" +
        "                        <p>Este es un correo electrónico automático; si lo recibiste por error, no tienes que hacer nada.</p>" +
        "                      </td>" +
        "                    </tr>" +
        "                  </table>" +
        "                </td>" +
        "              </tr>" +
        "" +
        "            <!-- END MAIN CONTENT AREA -->" +
        "            </table>" +
        "" +
        "  </body>" +
        "</html>";
}