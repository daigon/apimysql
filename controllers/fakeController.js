const User = require('../models/user');
const Image = require('../models/image');
const Idea = require('../models/idea');

var collArray;
var imgArray;


// metodos de actualización 
function updateImag(req, res) {
    var images = [];

    User.find()
        .then((users) => {
            Image.find({}, function (err, result) {
                images = result;

                for (let i = 0; i < users.length; i++) {

                    let id = users[Math.floor(Math.random() * users.length)]._id

                    Image.findByIdAndUpdate({
                        _id: images[i]._id
                    }, {
                        user: id
                    }, function (err, result) {
                        if (err) {
                            console.log(err);
                        }
                    });
                }
            })
        })
}

function updateId() {
    Idea.update({}, getIdeaprops())
        .then(Idea => console.log('entro2'));
}

function getIdeaprops() {
    return {
        author: getAuthor(),
        coach: getCoach(),
        collaborators: getCollaborator(),
        image: getImages()
    }
}

// Conusltas 
function getAuthor(req, res) {
    var authors = [];

    User.find()
        .then((users) => {
            Idea.find({}, function (err, result) {
                authors = result;

                for (let i = 0; i < users.length; i++) {

                    let id = users[Math.floor(Math.random() * users.length)]._id

                    Idea.findByIdAndUpdate({
                        _id: authors[i]._id
                    }, {
                        author: id
                    }, function (err, result) {
                        if (err) {
                            console.log(err);
                        }
                    });


                }
            })
        })
}

function getUser(req, res) {

}

function getCoach(req, res) {
    var ideas = [];

    User.find()
        .then((users) => {
            Idea.find({}, function (err, result) {
                ideas = result;

                for (let i = 0; i < users.length; i++) {

                    let id = users[Math.floor(Math.random() * users.length)]._id

                    Idea.findByIdAndUpdate({
                        _id: ideas[i]._id
                    }, {
                        coach: id,
                    }, function (err, result) {
                        if (err) {
                            console.log(err);
                        }
                    });


                }
            })

        })

}

function getCollaborator(req, res) {
    var i;
    var j;
    var id = [];
    User.find({
            ideaRole: 'COLLABORATOR'
        })
        .then((users) => {
            Idea.find({}, function (err, ideas) {
                collaborators = [];
                // inicio de ciclos 

                for (i = 0; i < 5; i++) {

                    collaborators = users[Math.floor(Math.random() * users.length)]._id
                    id.push(collaborators);
                }

                for (j = 0; j < ideas.length; j++) {

                    if (collaborators[j] == null) {

                        Idea.findByIdAndUpdate({
                            _id: ideas[j]._id
                        }, {
                            collaborator: id

                        }, function (err, ideas) {
                            if (err) {
                                console.log(err);
                            }
                        });
                    } else if (collaborators[j + 1] != collaborators[j]) {

                        Idea.findByIdAndUpdate({
                            _id: ideas[j]._id
                        }, {
                            collaborator: id

                        }, function (err, ideas) {
                            if (err) {
                                console.log(err);
                            }
                        });

                        // final de fors 

                    }
                }

            })

        })
}

function getImages(req, res) {
    var i;
    var j;
    var id = [];
    Image.find()
        .then((images) => {
            Idea.find({}, function (err, ideas) {
                imageArry = [];

                for (i = 0; i < 5; i++) {

                    imageArry = images[Math.floor(Math.random() * images.length)]._id
                    id.push(imageArry);
                }

                for (let j = 0; j < ideas.length; j++) {



                    if (imageArry[j] == null) {

                        Idea.findByIdAndUpdate({
                            _id: ideas[j]._id
                        }, {
                            image: id

                        }, function (err, ideas) {
                            if (err) {
                                console.log(err);
                            }
                        });
                    } else if (imageArry[j + 1] != imageArry[j]) {

                        Idea.findByIdAndUpdate({
                            _id: ideas[j]._id
                        }, {
                            image: id

                        }, function (err, ideas) {
                            if (err) {
                                console.log(err);
                            }
                        });


                    }

                }


            })

        })
}

module.exports = {
    updateImag,
    updateId

}