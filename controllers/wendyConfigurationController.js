const connection = require('../services/DbConnection');
const wendyConfiguration = connection.import('../models/wendyConfiguration');


module.exports = {

    deleteWendyModle(req, res, next) {
        try {
            var check = req.body.iswork;

            var area;
            var charge;

            wendyConfiguration.findOne({})
                .then(wendyConfigurationModel => {
                    if (check) {
                        area = req.body.area;
                
                        wendyConfigurationModel.workArea.pull(area);
                        return wendyConfiguration.findByIdAndUpdate({_id:wendyConfigurationModel._id},wendyConfigurationModel); 
                    } else {
                        charge = req.body.charge;

                        wendyConfigurationModel.workCharge.pull(charge);
                        return wendyConfiguration.findByIdAndUpdate({_id:wendyConfigurationModel._id},wendyConfigurationModel); 
                    }

                })
                .then(wendyConfigurationModel => {
                    return wendyConfiguration.findOne({});
                 })
                 .then(wendyConfigurationModel => {
                     reshelpobject(200, wendyConfigurationModel, res);
                 })
                .catch(err => {
                    reshelperr(500, err.message, res);
                })
        } catch (error) {
            console.log(error);
        }

    },

    updateWendyModel(req, res, next) {
        try {
            console.log("entro");
            var check = req.body.iswork;
            var container;
            var areaold;
            var areanew;
            var chargeold;
            var chargenew;

            wendyConfiguration.findOne({})
                .then(wendyConfigurationModel => {
                    if (check) {
                        areaold = req.body.areaold;
                        areanew = req.body.areanew;
                       container = wendyConfigurationModel.workArea;
                        for (let i = 0; i < container.length; i++) {
                            if (container[i] === areaold  ) {
                                 
                                wendyConfigurationModel.workArea[i] = areanew;
                                 return wendyConfiguration.findByIdAndUpdate({_id:wendyConfigurationModel._id},wendyConfigurationModel); 
                            }
                            
                        }
                    } else {
                        chargeold = req.body.chargeold;
                        chargenew = req.body.chargenew;

                        for (let i = 0; i < wendyConfigurationModel.workCharge.length; i++) {
                            if (chargeold === wendyConfigurationModel.workCharge[i]) {

                                wendyConfigurationModel.workCharge[i] = chargenew;
                                return wendyConfiguration.findByIdAndUpdate({_id:wendyConfigurationModel._id},wendyConfigurationModel); 
                                
                            }
                            
                        }
                    }
                })
                .then(wendyConfigurationModel => {
                   return wendyConfiguration.findOne({});
                })
                .then(wendyConfigurationModel => {
                    reshelpobject(200, wendyConfigurationModel, res);
                })
                .catch((err) => {
                    reshelperr(500, err.message, res);
                })

        } catch (err) {
            console.log(err);
        }
    },

    createWendyModel(req, res, next) {
        try {
            console.log("entro");
            var check = req.body.iswork;

            var area;
            var charge;
            wendyConfiguration.findOne()
                .then(wendyConfigurationModel => {

                    if (check) {
                        area = req.body.area;
                        var workAreas = JSON.parse(wendyConfigurationModel.workArea);
                        workAreas.push(area);
                        wendyConfigurationModel.workArea = JSON.stringify(workAreas);
                        return wendyConfigurationModel.save();
                    } else {
                        charge = req.body.charge;
                        var workCharges = JSON.parse(wendyConfigurationModel.workCharge);
                        workCharges.push(charge);
                        wendyConfigurationModel.workCharge = JSON.stringify(workCharges);

                       
                        return wendyConfigurationModel.save();
                    }

                })
                .then(wendyConfigurationModel => {
                    wendyConfigurationModel.workArea = JSON.parse(wendyConfigurationModel.workArea);
                    wendyConfigurationModel.workCharge = JSON.parse(wendyConfigurationModel.workCharge);
                    reshelpobject(200, wendyConfigurationModel, res);
                })
                .catch(err => {
                    reshelperr(500, err.message, res);

                })
        } catch (error) {
            console.log(error);
        }


    },

    getWroks(req, res, next) {
        wendyConfiguration.findOne({})
            .then((wendyConfigurationModel) => {
                wendyConfigurationModel.workArea = JSON.parse(wendyConfigurationModel.workArea);
                wendyConfigurationModel.workCharge = JSON.parse(wendyConfigurationModel.workCharge);
                reshelpobject(200, wendyConfigurationModel, res);
            })
            .catch(error => {
                reshelperr(500, error.message, res)
            });
    },

    createModel(req, res, next) {
        try {
            var modelWaCh = req.body;
            var areas = ['Calidad'];
            var charges = ['Gerente'];

            modelWaCh.workArea = JSON.stringify(areas);
            modelWaCh.workCharge = JSON.stringify(charges);
            console.log(modelWaCh)
            wendyConfiguration.create(modelWaCh)
                .then(wendyConfigurationModel => {
                    reshelpobject(200, wendyConfigurationModel, res);
                })
                .catch(err => {
                    reshelperr(500, err.message, res);
                    console.log(err.message)
                })

        } catch (err) {
            console.log(err);
        }
    }
}

function reshelperr(status, message, res) {
    res.status(status).send({
        message: message
    });
}

function reshelpobject(status, comment, res) {
    res.status(status).send(comment);
}