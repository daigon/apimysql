

const connection = require('../services/DbConnection');
const Idea = connection.import('../models/idea');
const comment = connection.import('../models/comment');
const Collaborator = connection.import('../models/collaborators');
const image = connection.import('../models/image');
const User = connection.import('../models/user');
const notify = connection.import('../models/notify');
const review = connection.import('../models/review');
const like = connection.import('../models/like');

const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const fs = require('fs');
const path = require('path');
var uuid = require('uuid');

const mongoosePaginate = require('mongoose-pagination');
const populateQuery = [
    { model: User, as: 'author' },
    { model: User, as: 'coach' },
    {
        model: Collaborator, as: 'collaborator',
        include: [{ model: User, as: 'info' }]
    },
    { model: notify, as: 'notify' },
    {
        model: image, as: 'image',
        include: [{ model: User, as: 'user' }]
    },
    {
        model: comment, as: 'comments',
        include: [{ model: User, as: 'user' }]
    },
    { model: review, as: 'review' },
    { model: like }
];
module.exports = {

    getIdea(req, res, next) {
        const _id = req.params.id;
        try {
            Idea.findById(_id,
                {
                    include: populateQuery
                })
                .then(idea => reshelpobject(200, idea, res))
                .catch(error => {
                    console.log("error => " + error.message)
                    console.log(error)
                    return reshelperr(500, error.message, res)
                });
        } catch (err) {
            console.log("error => " + err.message)
        }


    },

    createImage(req, res, next) {

        var imageProps = req.body;
        image.create(imageProps)
            .then(image => reshelpobject(200, image, res))
            .catch(error => {
                reshelperr(500, error.message, res)
            });
    },

    create(req, res, next) {
        try {
            var ideaProps = req.body;

            ideaProps['coachid'] = ideaProps.coach;
            ideaProps['authorid'] = ideaProps.author;

            var coach = ideaProps.coach;
            var collaborators = ideaProps.collaborator;
            var images = ideaProps.image
            delete ideaProps.image;
            delete ideaProps.collaborator;
            Idea.create(ideaProps)
                .then(idea => {
                    var collaboratorsMysql = [];
                    for (let i = 0; i < collaborators.length; i++) {
                        collaboratorsMysql.push({
                            ideaid: Number.parseInt(idea._id),
                            userid: Number.parseInt(collaborators[i])
                            // model_a_field: ...
                        });

                    }

                    Collaborator.bulkCreate(collaboratorsMysql)
                    image.update({ ideaId: idea._id },
                        {
                            where: {
                                _id: images
                            }
                        })
                    return Idea.findById(idea._id,
                        {

                        })


                })
                .then(idea => reshelpobject(200, idea, res))
                .catch(error => {
                    console.log(error)
                    return reshelperr(500, error.message, res)
                });



            User.findById(coach)
                .then(user => {

                    user.notifyAddIdea = true;
                    return user.save();
                })
                .then(user => {

                    if (user.notifyAddIdea == true) {

                    }

                })
                .catch(error => {
                    console.log(error)

                });

            for (let i = 0; i < collaborators.length; i++) {
                User.findById(collaborators[i])
                    .then(user => {

                        user.notifyAddIdea = true;
                        return user.save();
                    })
                    .then(user => {

                        if (user.notifyAddIdea == true) {

                        }

                    })
                    .catch(error => {
                        console.log(error)

                    });
            }


        }
        catch (err) {
            console.log("error => " + err.message);

        }

    },

    getIdeasBySearch(req, res, next) {
        try {
            var criteria = req.body.criteria.toUpperCase();
            var page = req.params.page;
            var role = req.body.role;
            if (!page) {
                page = 1;
            }
            var limit = 10;   // number of records per page
            var offset = 0;
            var query = {
                [Op.and]: [
                    { title: { [Op.like]: '%' + criteria + '%' } },
                    { isShow: true }
                ]

            }
            Idea.findAndCountAll({
                where: query
            })
                .then(result => {
                    pages = Math.ceil(result.count / limit);
                    offset = limit * (page - 1);
                    return Idea.findAll({
                        limit: limit,
                        offset: offset,
                        $sort: { date: 1 },
                        where: query,
                        include: populateQuery
                    })
                })
                .then(result => {

                    return reshelpobject(200, { ideas: result, pages }, res);
                })
                .catch(err => {
                    console.log(err);

                    return reshelperr(500, err.original.sqlMessage, res);
                });
        } catch (error) {
            console.log(error);
            return reshelperr(500, "Error en el servidor", res);
        }


    },

    getIdeasBySearchAndAuthor(req, res, next) {
        try {
            var criteria = req.body.criteria.toUpperCase();
            var page = req.body.page;

            var authorId = req.params.id;
            if (!page) {
                page = 1;
            }
            var limit = 10;   // number of records per page
            var offset = 0;
            var query = {
                [Op.and]: [{
                    title: {
                        [Op.like]: '%' + criteria + '%'
                    }
                },
                {
                    authorid: authorId
                },
                { isShow: true }
                ]
            }
            Idea.findAndCountAll({
                where: query
            })
                .then(result => {
                    pages = Math.ceil(result.count / limit);
                    offset = limit * (page - 1);
                    return Idea.findAll({
                        limit: limit,
                        offset: offset,
                        $sort: { date: 1 },
                        where: query,
                        include: populateQuery
                    })
                })
                .then(result => {

                    return reshelpobject(200, { ideas: result, pages }, res);
                })
                .catch(err => {
                    console.log(err);

                    return reshelperr(500, err.original.sqlMessage, res);
                });
        } catch (error) {
            console.log(error);
            return reshelperr(500, "Error en el servidor", res);
        }

    },

    getIdeasByAuthor(req, res, next) {
        try {
            var page = req.params.page;
            var authorId = req.params.author
            if (!page) {
                page = 1;
            }

            var limit = 10;   // number of records per page
            var offset = 0;
            var query = {
                [Op.and]: [
                    {
                        [Op.or]: [{
                            authorid: Number.parseInt(authorId)
                        },
                        {
                            coachid: Number.parseInt(authorId)
                        },
                            // {
                            //     collaborator.userid: { "$in": [authorId] }
                            // }
                        ]
                    }

                ]

            }


            Idea.findAndCountAll({ where: query })
                .then(result => {
                    pages = Math.ceil(result.count / limit);
                    offset = limit * (page - 1);
                    return Idea.findAll({
                        limit: limit,
                        offset: offset,
                        $sort: { date: 1 },
                        include: populateQuery,
                        where: query
                    })

                })
                .then(result => {

                    return reshelpobject(200, { ideas: result, pages }, res);
                })
                .catch(err => {
                    console.log(err);

                    return reshelperr(500, err.original.sqlMessage, res);
                });
        } catch (error) {
            console.log(error)
            return reshelperr(500, "Error en el servidor", res)
        }



    },

    //verificar que no sean falsas
    getIdeasByScore(req, res, next) {
        try {
            var page = req.params.page;
            if (!page) {
                page = 1;
            }

            var limit = 10;   // number of records per page
            var offset = 0;
            var query = { isShow: true }





            Idea.findAndCountAll()
                .then(result => {
                    pages = Math.ceil(result.count / limit);
                    offset = limit * (page - 1);
                    return Idea.findAll({
                        limit: limit,
                        offset: offset,
                        $sort: { score: 1 },
                        include: populateQuery,
                        where: query
                    })

                })
                .then(result => {

                    return reshelpobject(200, { ideas: result, pages }, res);
                })
                .catch(err => {
                    console.log(err);

                    return reshelperr(500, err.original.sqlMessage, res);
                });
        } catch (error) {
            console.log(error)
            return reshelperr(500, "Error en el servidor", res)
        }

    },

    getIdeasByDate(req, res, next) {
        try {
            var page = req.params.page;
            if (!page) {
                page = 1;
            }

            var limit = 10;   // number of records per page
            var offset = 0;
            var query = { isShow: true }


            Idea.findAndCountAll()
                .then(result => {
                    pages = Math.ceil(result.count / limit);
                    offset = limit * (page - 1);
                    return Idea.findAll({
                        limit: limit,
                        offset: offset,
                        $sort: { date: 1 },
                        include: populateQuery,
                        where: query
                    })

                })
                .then(result => {

                    return reshelpobject(200, { ideas: result, pages }, res);
                })
                .catch(err => {
                    console.log(err);

                    return reshelperr(500, err.original.sqlMessage, res);
                });
        } catch (error) {
            console.log(error)
            return reshelperr(500, "Error en el servidor", res)
        }

    },

    getIdeasByLikes(req, res, next) {
        try {
            var page = req.params.page;
            if (!page) {
                page = 1;
            }

            var limit = 10;   // number of records per page
            var offset = 0;
            var query = { isShow: true }


            Idea.findAndCountAll()
                .then(result => {
                    pages = Math.ceil(result.count / limit);
                    offset = limit * (page - 1);
                    return Idea.findAll({
                        limit: limit,
                        offset: offset,
                        $sort: { likes: 1 },
                        include: populateQuery,
                        where: query
                    })

                })
                .then(result => {

                    return reshelpobject(200, { ideas: result, pages }, res);
                })
                .catch(err => {
                    console.log(err);

                    return reshelperr(500, err.original.sqlMessage, res);
                });
        } catch (error) {
            console.log(error)
            return reshelperr(500, "Error en el servidor", res)
        }
    },

    update(req, res, next) {
        try {
            const _id = req.params.id;
            const ideaProps = req.body;
            var globalIdea;
            var images = ideaProps.image
            delete ideaProps.image;
            //
            var coach;
            var collaborators;
            Idea.update(ideaProps, {
                where: { _id: _id }
            })
                .then(() => {


                    return Idea.findById(_id, { include: populateQuery })

                })
                .then(idea => {
                    coach = idea.coach;
                    collaborators = idea.collaborator;
                    globalIdea = idea;

                    image.update({ ideaId: idea._id },
                        {
                            where: {
                                _id: images
                            }
                        })
                        .then(rows => console.log(rows))

                    reshelpobject(200, idea, res);

                    if (coach !== undefined && collaborators != undefined) {
                        if (globalIdea != null) {
                            updateEmployees(collaborators, coach);
                            if (globalIdea.notify.length > 0) {
                                updateNotify(globalIdea);
                            }
                            else {
                                AddNewNotify(globalIdea, coach, collaborators);
                            }

                        }

                    }
                }
                )
                .catch(error => {

                    return reshelperr(500, error.message, res);
                    console.log(e.message);
                });
        }
        catch (e) {
            console.log(e);
            return reshelperr(500, error.message, res);
        }




    },

    updateIsShow(req, res, next) {
        try {

            const ideaId = req.params.id;
            const ideaProps = req.body;

            Idea.update(ideaProps, {
                where: { _id: ideaId }
            }, )
                .then((idea) => {
                    return Idea.findById(ideaId, { include: populateQuery })

                })
                .then(idea => {
                    coach = idea.coach;
                    collaborators = idea.collaborator;
                    globalIdea = idea;

                    return reshelpobject(200, idea, res);

                }
                )
                .catch(error => {

                    return reshelperr(500, error.message, res)
                });
        } catch (error) {
            console.log(error);
            return reshelperr(500, "Error en el servidor", res);
        }
    },
    //borrar comentarios y reviews
    delete(req, res, next) {

        const ideaId = req.params.id;
        try {
            return Idea.destroy({
                where: {
                    _id: ideaId
                }
            })
                .then((affectedRows) => {
                    if (affectedRows)
                        return reshelperr(200, 'Idea eliminada', res)
                    else
                        return reshelperr(500, 'Error al eliminar la idea', res)

                })
                .catch(error => {
                    return reshelperr(500, error.message, res);

                });
        } catch (error) {
            console.log(error);
            return reshelperr(500, "Error en el servidor", res);
        }

    },

    uploadImage(req, res, next) {
        try {

        } catch (error) {
            console.log(error);
            reshelperr(500, "Error en el servidor", res);
        }
        //_id del usuario
        const _id = req.params.id;
        var file_name = 'no subido';

        if (req.files) {
            const file_path = req.files.image.path;
            var file_split = file_path.split('\\');
            var file_name = file_split[2];

            var ext_split = file_name.split('\.');
            var file_ext = ext_split[1];

            if (file_ext == 'png' || file_ext == 'jpg' || file_ext == 'gif') {
                image.findByIdAndUpdate(_id, {
                    image: file_name
                })
                    .then(() => image.findById({
                        _id
                    }))
                    .then(idea => {
                        if (idea !== null)
                            reshelpobject(200, idea, res);
                        else
                            reshelperr(500, 'No se encontro el usuario', res);

                    })
                    .catch(error => {
                        reshelperr(500, error.message, res)
                    });
            } else {
                reshelperr(500, 'Extension del archivo no valida', res);

            }

        } else {
            reshelperr(403, 'No has subido una imagen ...', res);

        }
    },

    uploadImageMovil(req, res, next) {

        try {

            var imageName = uuid.v1() + '.jpg';
            var path = "uploads/ideas/" + imageName;
            var file = fs.writeFileSync(path, req.text);

            newImage = {};
            newImage.image = imageName;
            newImage.user = req.headers.ui;
            newImage.comment = req.headers.comment;
            newImage.stage = req.headers.stage;

            image.create(newImage)
                .then(image => reshelpobject(200, image, res))
                .catch(error => {

                    return reshelperr(500, error.message, res)
                });


        }
        catch (e) {

            console.log("error => " + e)
            return reshelperr(500, 'Error en el servidor', res)
        }

    },

    getImageFile(req, res, next) {
        try {

            const imageFile = req.params.imageFile;
            const pathfile = './uploads/Ideas/' + imageFile;

            fs.exists(pathfile, (exists) => {
                if (exists)
                    return res.sendFile(path.resolve(pathfile));
                else
                    return reshelperr(403, 'No has subido una imagen ...', res);
            });
        } catch (error) {
            console.log(error);
            return reshelperr(500, "Error en el servidor", res);
        }
    },

    getIdeasBySearchWithoutPagination(req, res, next) {
        try {


            var criteria = req.body.criteria;
            var page = req.params.page;


            if (!page)
                page = 1;


            var itemsPage = 9;
            const query =
                Idea.find({
                    title: {
                        $regex: new RegExp(criteria, "i")
                    }
                })
                    .sort({
                        score: -1
                    })
                    .populate(populateQuery);

            Promise.all([query, Idea.count({
                title: {
                    $regex: new RegExp(criteria, "i")
                }
            })])
                .then((results) => {
                    const object = {
                        pages: results[1],
                        ideas: results[0]
                    };
                    return reshelpobject(200, object, res);
                })
                .catch(error => {
                    return reshelperr(500, error.message, res)
                });

        } catch (error) {
            console.log(error);
            return reshelperr(500, "Error en el servidor", res);
        }
    },

    getIdeasBySearchAndAuthorWithoutPagination(req, res, next) {
        try {


            var criteria = req.body.criteria.toUpperCase();
            var authorId = req.params.id;
            var page = req.body.page;
            if (!page)
                page = 1;


            var itemsPage = 10;

            var query = Idea.find({
                $and: [{
                    title: {
                        $regex: new RegExp(criteria, "i")
                    }
                },
                {
                    author: authorId
                }
                ]
            })
                .sort({
                    score: -1
                })
                .populate(populateQuery);


            Promise.all([query, Idea.count({
                $and: [{
                    title: {
                        $regex: new RegExp(criteria, "i")
                    }
                },
                {
                    author: authorId
                }
                ]
            })])
                .then((results) => {
                    const object = {
                        pages: results[1],
                        ideas: results[0]
                    };
                    return reshelpobject(200, object, res);
                })
                .catch(error => {
                    return reshelperr(500, error.message, res)
                });
        } catch (error) {
            console.log(error);
            return reshelperr(500, "Error en el servidor", res);
        }

    },

    getIdeasByAuthorWithoutPagination(req, res, next) {
        try {
            var page = req.params.page;
            var authorId = req.params.author
            if (!page) {
                page = 1;
            }

            var limit = 10;   // number of records per page
            var offset = 0;
            var query = {
                [Op.or]: [{
                    authorid: Number.parseInt(authorId)
                },
                {
                    coachid: Number.parseInt(authorId)
                },
                    // {
                    //     collaborator.userid: { "$in": [authorId] }
                    // }
                ]
            }


            Idea.findAndCountAll({ where: query })
                .then(result => {
                    pages = Math.ceil(result.count / limit);
                    offset = limit * (page - 1);
                    return Idea.findAll({
                        // limit: limit,
                        // offset: offset,
                        $sort: { date: 1 },
                        include: populateQuery,
                        where: query
                    })

                })
                .then(result => {

                    return reshelpobject(200, { ideas: result, pages }, res);
                })
                .catch(err => {
                    console.log(err);

                    return reshelperr(500, err.original.sqlMessage, res);
                });
        } catch (error) {
            console.log(error)
            return reshelperr(500, "Error en el servidor", res)
        }


    },
    //verificar que no sean falsas
    getIdeasByScoreWithoutPagination(req, res, next) {
        try {
            var page = req.params.page;
            var authorId = req.params.author
            if (!page) {
                page = 1;
            }

            var limit = 10;   // number of records per page
            var offset = 0;
            var query = { isShow: true }


            Idea.findAndCountAll({ where: query })
                .then(result => {
                    pages = Math.ceil(result.count / limit);
                    offset = limit * (page - 1);
                    return Idea.findAll({
                        // limit: limit,
                        // offset: offset,
                        $sort: { date: 1, score: 1 },
                        include: populateQuery,
                        where: query
                    })

                })
                .then(result => {

                    return reshelpobject(200, { ideas: result, pages }, res);
                })
                .catch(err => {
                    console.log(err);

                    return reshelperr(500, err.original.sqlMessage, res);
                });
        } catch (error) {
            console.log(error)
            return reshelperr(500, "Error en el servidor", res)
        }
    },

    getIdeasByDateWithoutPagination(req, res, next) {
        try {
            var page = req.params.page;
            var authorId = req.params.author
            if (!page) {
                page = 1;
            }

            var limit = 10;   // number of records per page
            var offset = 0;
            var query = { isShow: true }


            Idea.findAndCountAll({where:query})
                .then(result => {
                    pages = Math.ceil(result.count / limit);
                    offset = limit * (page - 1);
                    return Idea.findAll({
                        // limit: limit,
                        // offset: offset,
                        $sort: { date: 1 },
                        include: populateQuery,
                        where: query
                    })

                })
                .then(result => {

                    return reshelpobject(200, { ideas: result, pages }, res);
                })
                .catch(err => {
                    console.log(err);

                    return reshelperr(500, err.original.sqlMessage, res);
                });
        } catch (error) {
            console.log(error)
            return reshelperr(500, "Error en el servidor", res)
        }
    },

    getIdeasByLikesWithoutPagination(req, res, next) {
        try {
            var page = req.params.page;
            var authorId = req.params.author
            if (!page) {
                page = 1;
            }

            var limit = 10;   // number of records per page
            var offset = 0;
            var query = { isShow: true }


            Idea.findAndCountAll({ where: query })
                .then(result => {
                    pages = Math.ceil(result.count / limit);
                    offset = limit * (page - 1);
                    return Idea.findAll({
                        // limit: limit,
                        // offset: offset,
                        $sort: { date: 1 },
                        include: populateQuery,
                        where: query
                    })

                })
                .then(result => {

                    return reshelpobject(200, { ideas: result, pages }, res);
                })
                .catch(err => {
                    console.log(err);

                    return reshelperr(500, err.original.sqlMessage, res);
                });
        } catch (error) {
            console.log(error)
            return reshelperr(500, "Error en el servidor", res)
        }
    },

    getTitlesIdeas(req, res, next) {
        try {
            const query =
                Idea.findAll({ $sort: { title: -1 }, include: populateQuery })
                    .then((ideas) => {
                        return reshelpobject(200, { ideas }, res);
                    })
                    .catch(error => {
                        return reshelperr(500, error.message, res)
                    });
        }
        catch (e) {
            console.log("error => " + err.message)
            return reshelperr(500, 'Error en el servidor', res)
        }
    }
};

function updateEmployees(collaborators, coach) {

    User.findById(coach._id)
        .then(user => {

            user.notifyUpdateIdea = true;
            return user.save();
        })
        .then(user => {

            if (user.notifyUpdateIdea == true) {

            }

        })
        .catch(error => {
            console.log("error => " + err.message)

        });

    for (let i = 0; i < collaborators.length; i++) {
        User.findById(collaborators[i].info._id)
            .then(user => {

                user.notifyUpdateIdea = true;
                return user.save();
            })
            .then(user => {

                if (user.notifyUpdateIdea == true) {

                }

            })
            .catch(error => {

                console.log("error => " + err.message)
            });
    }
}


function AddNewNotify(idea, coach, collaborators) {
    try {
        var Notify = {};
        Notify.user = coach._id;
        Notify.notify = true;
        Notify.ideaId = idea._id;
        notify.create(Notify)
            .then(object => {
                // idea.notify.push(object);

                for (var i = 0; i < collaborators.length; i++) {
                    Notify = {};
                    Notify.user = collaborators[i].info._id;
                    Notify.notify = true;
                    Notify.ideaId = idea._id;

                    notify.create(Notify)
                        .then(object => {
                            // idea.notify.push(object);
                            // if (idea.notify.length === (collaborators.length + 1)) {
                            //     Idea.update(idea, {
                            //         where: { _id: idea.id }
                            //     })
                            //         .then(idea => { })
                            //         .catch(err => console.log("error => " + err.message));
                            // }
                        })
                        .catch(err => console.log("error => " + err.message));
                }
            })
            .catch(err => console.log("error => " + err.message));

    }
    catch (e) {
        console.log("error => " + e);
    }
}

function updateNotify(idea) {

    try {
        // for (let i = 0; i < idea.notify.length; i++) {
        //     idea.notify[i].notify = true;
        //     Idea.update(idea, { where: { _id: idea.id } })

        notify.update({ notify: true }, { where: { ideaId: idea._id } })
            .then(idea => { })
            .catch(err => console.log("error => " + err.message));
        //}

    }
    catch (e) {
        console.log("error => " + e);
    }
}

function reshelperr(status, message, res) {
    res.status(status).send({
        message: message
    });
}

function reshelpobject(status, album, res) {
    res.status(status).send(album);
}