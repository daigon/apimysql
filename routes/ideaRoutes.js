const IdeaController = require('../controllers/ideaController');
const CommentController = require('../controllers/commentController');
const md_auth = require('../middlewares/authenticated');

const multipart = require('connect-multiparty');
const md_upload = multipart({
    uploadDir: './uploads/ideas'
});
const dataUpdate = require('../controllers/fakeController');
const bodyParser = require('body-parser');
var rawParser = bodyParser.raw({
    type: 'application/octet-stream',
    extended: true
});

module.exports = (app) => {
    //poner verificacion de token despues de las pruebas
    app.post('/api/idea/register', md_auth.ensureAuth, IdeaController.create); //echo
    app.post('/api/idea/image', md_auth.ensureAuth, IdeaController.createImage); //echo
    app.put('/api/idea/:id', md_auth.ensureAuth, IdeaController.update); //echo
    app.put('/api/idea/isShow/:id', md_auth.ensureAuth, IdeaController.updateIsShow); //echo
    app.delete('/api/idea/:id', md_auth.ensureAuth, IdeaController.delete); //echo
    app.get('/api/idea/:id', md_auth.ensureAuth, IdeaController.getIdea); //echo
    //md_auth.ensureAuth.ensureAuth,md_auth.ensureAuth,
    app.get('/api/ideas/score/:page?', IdeaController.getIdeasByScore); //echo
    app.post('/api/ideas/author/:author', md_auth.ensureAuth, IdeaController.getIdeasByAuthor); //echo
    app.get('/api/ideas/date/:page?', md_auth.ensureAuth, IdeaController.getIdeasByDate); //echo
    app.get('/api/ideas/likes/:page?', md_auth.ensureAuth, IdeaController.getIdeasByLikes);
    app.get('/api/ideas/titles/', md_auth.ensureAuth, IdeaController.getTitlesIdeas);
    // falta hacerlo
    app.post('/api/ideas/search/:page?', md_auth.ensureAuth, IdeaController.getIdeasBySearch);
    app.post('/api/ideas/search/author/:id', md_auth.ensureAuth, IdeaController.getIdeasBySearchAndAuthor);


    //md_auth.ensureAuth.ensureAuth,
    app.get('/api/movil/ideas/score/:page?', md_auth.ensureAuth, IdeaController.getIdeasByScoreWithoutPagination); //echo
    app.post('/api/movil/ideas/author/:author', md_auth.ensureAuth, IdeaController.getIdeasByAuthorWithoutPagination); //echo
    app.get('/api/movil/ideas/date/:page?', md_auth.ensureAuth, IdeaController.getIdeasByDateWithoutPagination); //echo
    app.get('/api/movil/ideas/likes/:page?', md_auth.ensureAuth, IdeaController.getIdeasByLikesWithoutPagination);


    app.get('/api/idea/get-image-idea/:imageFile', IdeaController.getImageFile); //echo
    app.post('/api/idea/upload-image-idea/:id', [md_auth.ensureAuth, md_upload], IdeaController.uploadImage); //echo

    app.post('/api/idea/upload-image-ideaMovil/', IdeaController.uploadImageMovil);

    app.post('/api/idea/comments/:id', md_auth.ensureAuth, CommentController.create); //echo

    app.post('/api/idea/likes/:id', md_auth.ensureAuth, CommentController.updateLikes); //echo
    app.put('/api/idea/likes/:id', md_auth.ensureAuth, CommentController.removeLikes); //echo

    app.put('/api/idea/updateNotify/:id', md_auth.ensureAuth, CommentController.updateNotify); //echo

    app.post('/api/idea/review/:id', md_auth.ensureAuth, CommentController.createReview); //echo


    app.get('/ai', md_auth.ensureAuth, dataUpdate.updateImag);
    app.get('/aid', md_auth.ensureAuth, dataUpdate.updateId);
};