const UserController = require('../controllers/userController');
const md_auth = require('../middlewares/authenticated');
const multipart = require('connect-multiparty');
const md_upload = multipart({
    uploadDir: './uploads/users'
});

module.exports = (app) => {

    app.post('/api/user/register',md_auth.ensureAuth, UserController.create); //echo
    app.put('/api/user/update/:id',md_auth.ensureAuth, UserController.update); //echo
    app.delete('/api/user/:id', md_auth.ensureAuth, UserController.delete); //echo
    app.get('/api/user/:id',md_auth.ensureAuth, UserController.getUser);
    app.get('/api/user/get-image-user/:imageFile', UserController.getImageFile); //echo
    app.post('/api/user/upload-image-user/:id', [md_auth.ensureAuth, md_upload], UserController.uploadImageWeb); //echo

    app.post('/api/web', UserController.loginWeb); //echo
    app.post('/api/movil', UserController.loginMovil); //echo

    app.get('/api/users/:page?',md_auth.ensureAuth, UserController.getUsers); //echo
    app.get('/api/coach/:page?', md_auth.ensureAuth, UserController.getCoachs); //echo
    app.get('/api/colaborator/:page?', md_auth.ensureAuth, UserController.getCollaborators); //echo
    app.post('/api/users/search/:page?', md_auth.ensureAuth, UserController.getUsersBySearch); //echo
    app.post('/api/users/searchRole/:page?', md_auth.ensureAuth, UserController.getUsersBySearchAndRole); //echo

    app.post('/api/user/verifyData', md_auth.ensureAuth, UserController.verifyIdUser);
    app.post('/api/user/verifyEmail', md_auth.ensureAuth, UserController.verifyEmail);

    app.put('/api/user/update/:id',md_auth.ensureAuth, UserController.update); //echo
    app.delete('/api/user/:id', md_auth.ensureAuth, UserController.delete); //echo
    app.get('/api/user/:id',md_auth.ensureAuth, UserController.getUser);
    app.post('/api/user/verifyData', md_auth.ensureAuth, UserController.verifyIdUser);
    app.post('/api/user/verifyEmail', md_auth.ensureAuth, UserController.verifyEmail);
    app.get('/api/user/get-image-user/:imageFile', UserController.getImageFile); //echo
    app.post('/api/user/upload-image-user/:id', [md_auth.ensureAuth, md_upload], UserController.uploadImageWeb); //echo


    app.post('/api/user/forgot', UserController.forgotPassword);
    app.get('/api/verify/:token', UserController.verifyToken);
    app.post('/api/reset/:token', UserController.resetPassword);
};