const wendyConfiguartionController = require('../controllers/wendyConfigurationController');
const md_auth = require('../middlewares/authenticated');
//----->  seeds so that the user add, delete or update a workArea or workCharge <-----//

module.exports = (app) => {

    app.put('/api/wendy/update/', wendyConfiguartionController.updateWendyModel);
    app.post('/api/wendy/delete/', wendyConfiguartionController.deleteWendyModle);
    app.post('/api/wendy/create/',wendyConfiguartionController.createWendyModel);
    app.post('/api/wendy/desing/', wendyConfiguartionController.createModel);
    app.get('/api/wendy/getworks/',wendyConfiguartionController.getWroks);

    app.post('/api/wendy/desing/', wendyConfiguartionController.createModel);

}