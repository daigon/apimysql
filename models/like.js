'use strict'
const connection = require('../services/DbConnection');
const Idea = connection.import('./idea');
module.exports = (sequelize, DataTypes) => {
    const Like = sequelize.define('like', {
        user: {
            type: DataTypes.STRING(50),
            allowNull: false,
        }
    });

    Like.belongsTo(Idea,{ onDelete: 'CASCADE'})
    Idea.hasMany(Like)
    Like.sync();
    return Like;
}