'use strict'
const Sequelize = require('sequelize');
const connection = require('../services/DbConnection');
const User = connection.import('./user');
const Idea = connection.import('./idea');

module.exports = (sequelize, DataTypes) => {
    const Comments = sequelize.define('comment', {
        _id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        content: {
            type: DataTypes.STRING(50),
            allowNull: false
        },
        date: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            allowNull: false
        },

    });

    Comments.belongsTo(User, { as: 'user', onDelete: 'CASCADE' })
    Comments.belongsTo(Idea, {  onDelete: 'CASCADE' })

    User.hasMany(Comments,{as: 'comments'})
    Idea.hasMany(Comments,{as: 'comments'})

    Comments.sync();

    return Comments;
}