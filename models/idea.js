'use strict'
const connection = require('../services/DbConnection');
const User = connection.import('./user');
const Review = connection.import('./review');


module.exports = (sequelize, DataTypes) => {
    const Idea = sequelize.define('idea', {
        _id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        title: {
            type: DataTypes.STRING(50),
            allowNull: false,
            unique: 'uk_title'
        },
        problem: {
            type: DataTypes.STRING(50),
            defaultValue: null
        },
        description: {
            type: DataTypes.STRING(50),
            defaultValue: null
        },
        result: {
            type: DataTypes.STRING(50),
            defaultValue: null
        },
        experiment: {
            type: DataTypes.STRING(50),
            defaultValue: null
        },
        verify: {
            type: DataTypes.BOOLEAN,
            defaultValue: null
        },
        implementation: {
            type: DataTypes.STRING(50),
            defaultValue: null
        },
        startdate: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: DataTypes.NOW
        },
        score: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        status: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        isShow: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        // likes: {
        //     type: DataTypes.TEXT,
        // },
        stage1: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        stage2: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        stage3: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        stage4: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        stage5: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        problemTemp: {
            type: DataTypes.STRING(50),
            defaultValue: null
        },
        descriptionTemp: {
            type: DataTypes.STRING(50),
            defaultValue: null
        },
        resultTemp: {
            type: DataTypes.STRING(50),
            defaultValue: null
        },
        experimentTemp: {
            type: DataTypes.STRING(50),
            defaultValue: null
        },
        implementationTemp: {
            type: DataTypes.STRING(50),
            defaultValue: null
        },
    });
    Idea.belongsTo(User, { as: 'coach', foreignKey: 'coachid', onDelete: 'NO ACTION' })
    Idea.belongsTo(User, { as: 'author', foreignKey: 'authorid', onDelete: 'NO ACTION' })
    Idea.belongsTo(Review, {as: 'review',foreignKey: 'reviewid', onDelete: 'CASCADE'})
    // N:M
    Idea.sync();
    return Idea;
}