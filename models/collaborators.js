'use strict'
const connection = require('../services/DbConnection');
const User = connection.import('./user');
const Idea = connection.import('./idea');
module.exports = (sequelize, DataTypes) => {
  const Collaborators = sequelize.define('collaborator', {
  });
  // User.belongsToMany(Idea,
  //   {
  //     as: 'collaborators',
  //     through: { model: Collaborators, unique: false },
  //     foreignKey: 'ideaid'
  //   }
  // );
  // Idea.belongsToMany(User,
  //   {
  //     as: 'collaborators',
  //     through: { model: Collaborators, unique: false },
  //     foreignKey: 'userid'
  //   }
  // );

  Idea.hasMany(Collaborators, { as: 'collaborator', foreignKey: 'ideaid' } );
  User.hasMany(Collaborators, {as: 'info', foreignKey: 'userid'});
  Collaborators.belongsTo(User, {as: 'info', foreignKey: 'userid'});
  Collaborators.belongsTo(Idea, {as: 'collaborator', foreignKey: 'ideaid'});
  Collaborators.sync();
  return Collaborators;
}