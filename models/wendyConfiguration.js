'use strict'
module.exports = (sequelize, DataTypes) => {
    const Wendy = sequelize.define('wendyConfiguration', {
        _id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        workCharge: {
            type: DataTypes.TEXT,
            allowNull: true,
        },
        workArea:{
            type: DataTypes.TEXT,
            allowNull: true,
        },
    });
    Wendy.sync();
    return Wendy;
}