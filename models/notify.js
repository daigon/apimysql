'use strict'
const connection = require('../services/DbConnection');
const Idea = connection.import('./idea');
module.exports = (sequelize, DataTypes) => {
    const Notify = sequelize.define('notify', {
        _id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        user: {
            type: DataTypes.STRING(50),
            allowNull: false,
        },
        notify:{
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        stage:{
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0
        }
    });
    Notify.belongsTo(Idea, { onDelete: 'CASCADE' })
    Idea.hasMany(Notify,{as: 'notify'})
    Notify.sync();
    return Notify;
}