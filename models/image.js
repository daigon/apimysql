'use strict'
const Sequelize = require('sequelize');
const connection = require('../services/DbConnection');
const User = connection.import('./user');
const Idea = connection.import('./idea');
module.exports = (sequelize, DataTypes) => {
    const Image = sequelize.define('image', {
        _id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        image: {
            type: DataTypes.STRING(50),
            allowNull: false,
            defaultValue: 'user.png'
        },
        comment: {
            type: DataTypes.STRING(50),
            allowNull: false,
            defaultValue: ''
        },
        stage: {
            type: DataTypes.INTEGER,
            allowNull: false
        }

    });
   
    Image.belongsTo(User, {as: 'user', onDelete: 'CASCADE' })
    Image.belongsTo(Idea, { onDelete: 'CASCADE' })

    User.hasMany(Image,{as: 'user'})
    Idea.hasMany(Image,{as: 'image'})

    Image.sync();

    return Image;
}