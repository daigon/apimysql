'use strict'
const connection = require('../services/DbConnection');

module.exports = (sequelize, DataTypes) => {
    const Review = sequelize.define('review', {
        _id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        user: {
            type: DataTypes.STRING(50),
            allowNull: false,
        },
        teamFeel:{
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        safety:{
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        ergonomics:{
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        customer:{
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        score:{
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0
        }
    });
   
    // Idea.hasMany(Review,{as: 'review'})
    Review.sync();
    return Review;
}