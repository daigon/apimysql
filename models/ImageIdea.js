// 'use strict'
// const connection = require('../services/DbConnection');
// const Image = connection.import('./image');
// const Idea = connection.import('./idea');
// module.exports = (sequelize, DataTypes) => {
//     const Images = sequelize.define('imageidea', { 
//        id:{
//         type: DataTypes.INTEGER,
//         primaryKey: true,
//         autoIncrement: true // Automatically gets converted to SERIAL for postgres
//        }
//     });
//     // Idea.belongsTo(User, { as: 'coach', onDelete: 'SET NULL' })
//     // Idea.belongsTo(User, { as: 'collaborator', onDelete: 'SET NULL' })
//     // N:M
//     // Idea.belongsToMany(Image, {
//     //     type: DataTypes.INTEGER,
//     //     through: Images,
//     //     foreignKey: 'userid',
//     // });
    
//     // Image.belongsToMany(Idea, {
//     //     type: DataTypes.INTEGER,
//     //     through: Images,
//     //     foreignKey: 'imageid',
//     // });
//     Images.belongsToMany(Idea, { through: 'ideaid', foreignKey: '_id', onDelete: 'SET NULL' });
//     Images.belongsToMany(Image, { through: 'imageid', foreignKey: '_id', onDelete: 'SET NULL' });
//     Images.sync();
//     return Images;
// }