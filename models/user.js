'use strict'
const Sequelize = require('sequelize');
const connection = require('../services/DbConnection');



module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('user', {
       _id:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true // Automatically gets converted to SERIAL for postgres
       },
        idEmployee: {    // numeros 
            type: DataTypes.STRING(50),
            allowNull: false,
            unique: 'idEmployee'
        },
        name: {
            type: DataTypes.STRING(50),
            allowNull: false
        },
        surname: {
            type: DataTypes.STRING(50),
            allowNull: false
        },
        password: {       // numeros 
            type: DataTypes.STRING(255),
            allowNull: false
        },
        fullname: {
            type: DataTypes.STRING(50),
            allowNull: false
        },
        email: {
            type: DataTypes.STRING(50),
            allowNull: true,
            unique: 'emailIndex'
        },
        workArea: {
            type: DataTypes.STRING(50),
            allowNull: false
        },
        workCharge: {
            type: DataTypes.STRING(50),
            allowNull: false
        },
        startDate: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        systemRole: {                                // ROLE_USER, ROLE_ADMIN
            type: DataTypes.STRING(50),
            allowNull: false
        },
        ideaRole: {                                   // COLLABORATOR O COACH o BOTH
            type: DataTypes.STRING(50),
            allowNull: false
        },
        phone: {
            type: DataTypes.STRING(50),
            allowNull: false
        },
        image: {
            type: DataTypes.STRING(50),
            allowNull: false,
            defaultValue: 'user.png'
        },
        resetPasswordToken: {
            type: DataTypes.STRING(50),
        },
        resetPasswordExpires: {
            type: DataTypes.DATE,
           
        },
        notifyAddIdea: { 
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        notifyUpdateIdea: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    });
   
    User.sync();

    return User;
}