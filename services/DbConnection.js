config = require('../config');
const Sequelize = require('sequelize');
module.exports =  new Sequelize(config.db.db, config.db.user, config.db.pass, {
    host: 'sistemapresto.com',
    dialect: 'mysql',
    port: 3306,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    logging: false,
    operatorsAliases: false
});