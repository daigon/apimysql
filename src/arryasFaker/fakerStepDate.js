
var stpD1;
var stpD2;
var stpD3;
var stpD4;
var stpD5;

function sendStepDate1() {
     return stpD1 = [
        new Date(2017,09,25),
        new Date(2017,09,28),
        new Date(2017,09,30),
        new Date(2017,10,01),
        new Date(2017,10,03)
    ]
}

function sendStepDate2() {
    return  stpD2 = [
        new Date(2017,10,10),
        new Date(2017,10,18),
        new Date(2017,10,20),
        new Date(2017,10,28),
        new Date(2017,10,30)
    ]
}

function sendStepDate3() {
    return  stpD3 = [
        new Date(2017,11,01),
        new Date(2017,11,05),
        new Date(2017,11,10),
        new Date(2017,11,29),
        new Date(2017,12,01)
    ]
}

function sendStepDate4() {
    return  stpD4 = [
        new Date(2017,12,06),
        new Date(2017,12,17),
        new Date(2017,12,21),
        new Date(2017,12,28),
        new Date(2017,12,30)
    ]
}

function sendStepDate5() {
    return  stpD5 = [
        new Date(2017,12,31),
        new Date(2018,01,12),
        new Date(2018,01,19),
        new Date(2018,01,28),
        new Date(2018,01,30)
    ]
}

module.exports = {   
    sendStepDate1,
    sendStepDate2,
    sendStepDate3,
    sendStepDate4,
    sendStepDate5
}