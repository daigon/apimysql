const mongoose = require('mongoose'); // tomo el modelo 
const uuidv4 = require('uuid/v4'); // generador de idetidficadores unicos
const faker = require('faker');
const _ = require('lodash');
const config = require('../config');
// importaciones Controllers
const create = require('../controllers/userController');

// importaciones faker 
const fakeSystemRole = require('./arryasFaker/fakeSystemRole');
const fakeideaRol = require('./arryasFaker/fakeideaRol');

const fakeTitle = require('./arryasFaker/fakeTitle');
const stepDate = require('./arryasFaker/fakerStepDate');
const fakeIsShow = require('./arryasFaker/fakeIsShow');
const Idea = require('../models/user');
var fullname = "";
// users 
const MINIMUM_USERS = 2000;
const USERS_TO_ADD = 3000;
// images
const MINIMUM_IMAGES = 2000;
const IMAGES_TO_ADD = 3000;
//ideas
const MINIMUM_IDEAS = 2000;
const IDEAS_TO_ADD = 3000;

// vaeriables 
let usersCollaction;
let imageCollection;
let ideasCollection;
// conexion a la BD 
var uri = config.db;
mongoose.connect(uri, {
    useMongoClient: true,
  })
  .on('error', console.error.bind(console, 'connection error:'))
  .once('open', function () {
    console.log('open')
  })
  .then(() => {
   
    // obtenemos el modelo con mongoose 
    usersCollaction = mongoose.connection.collection("users")
    
    imageCollection = mongoose.connection.collection("images")
    ideasCollection = mongoose.connection.collection("ideas")
    


    return usersCollaction.count()



  })
  .then((users) => {
    

    if (users < MINIMUM_USERS) {
     
      const user = _.times(USERS_TO_ADD, () => (createFake()));

      usersCollaction.insertMany(user);
    }
    return imageCollection.count()
  })
  .then((image) => {
    
    
    if (image < MINIMUM_IMAGES) {
    const image = _.times(IMAGES_TO_ADD, () => (imageFake()));
    imageCollection.insertMany(image);
    return ideasCollection.count()
    }
  })
  .then((ideas) => {
   
    if (ideas < MINIMUM_IDEAS) {
    const ideas = _.times(IDEAS_TO_ADD, () => (ideasFake()));
    ideasCollection.insertMany(ideas);
    }
    console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>> entro 7 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<');
  })
  .catch(e => console.log(e));
// funcion la cual regresa un usuario fake 
function createFake() {
  return {
    idEmployee: uuidv4(), //Id 
    password: faker.internet.password(), // password
    name: nameFake(), //name
    surname: surnameFake(), //este
    fullname: Fullname() + clerFullName(),
    email: faker.internet.email(), //email
    workArea: faker.name.jobArea(), //este
    workCharge: faker.name.jobTitle(), //este
    startDate: new Date(2017, 9, 15), // date 
    systemRole: getfakeSystemRole(), // ROLE_USER, ROLE_ADMIN falta
    ideaRole: getfakeideaRol(), // COLLABORATOR O COACH
    phone: faker.phone.phoneNumber(), //telefono 
    image: faker.image.avatar() // imagen de perfil 
  };
}

function nameFake() {
  var name = faker.name.firstName().toUpperCase();
  fullname +=name + ' ';
  return name;
}

function surnameFake() {
  var surname = faker.name.lastName().toUpperCase();
  fullname +=surname;
  return surname;
}

function clerFullName(){
  fullname = "";
  return "";
}

function Fullname() {
  return fullname;
}

function imageFake() {
  return {
    image: faker.image.business(),
    comment: faker.lorem.paragraph(),
    user: null
  }
}

function getSchema(){
  review = mongoose.Schema({
    text:string
  })

  review = {text:faker.lorem.lines()};
  return review;
}
function ideasFake() {
  return {
    title: getfakeTitle(),
    image: null,
    problem: faker.lorem.paragraph(),
    description: faker.lorem.paragraph(),
    result: faker.lorem.lines(),
    experiment: faker.lorem.paragraph(),
    verify: faker.lorem.lines(),
    implementation: faker.lorem.paragraph(),
    startdate: new Date(2017, 09, 21),
    score: randomBetween(0, 500),
    likes: randomBetween(0, 5000),
    status: randomBetween(1, 3),
    isShow: getfakeIsShow(),
    author: null,
    coach: null,
    collaborator: null,
    comments: null,
    review: '',
    step1date: getStepDate(stepDate.sendStepDate1()),
    step2date: getStepDate(stepDate.sendStepDate2()),
    step3date: getStepDate(stepDate.sendStepDate3()),
    step4date: getStepDate(stepDate.sendStepDate4()),
    step5date: getStepDate(stepDate.sendStepDate5())
  }
}




function getfakeideaRol() {
  return randomEntry(fakeideaRol)
}

function getfakeIsShow() {
  return randomEntry(fakeIsShow)
}

function getfakeTitle() {
  return randomEntry(fakeTitle)
}

function getStepDate(args) {
  return randomEntry(args)
}

function getfakeSystemRole() {
  return randomEntry(fakeSystemRole);
}

function randomEntry(array) {
  return array[~~(Math.random() * array.length)];
}

function randomBetween(min, max) {
  return ~~(Math.random() * (max - min)) + min;
}