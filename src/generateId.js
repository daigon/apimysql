const fakeController = require('../controllers/fakeController');

var arraysCollaborator = fakeController.getCollaborator();
var arraysUser = fakeController.getUser();
var arraysCoach = fakeController.getCoach();
var arrayImages = fakeController.getImages();
// variables para guardar arrays 
var collArray;
var coaArray;
var imgArray;
var userArry;
// funcion la cual se ejecutara dentro de una funcion para obtener un Id distinto cada ves que se ejecute 
function IdRandom()  {
    const idCollaborator = arraysCollaborator[Math.floor(Math.random() * arraysCollaborator.length)]._id;
    const idCoach = arraysCoach[Math.floor(Math.random() * arraysCoach.length)]._id;
    const idImages = arrayImages[Math.floor(Math.random() * arrayImages.length)]._id;

}

function sendColloborator() {

    for (let i = 0; i < 5; i++) {
        IdRandom();

        if (arraysCollaborator[i] == null) {
            collArray.push(idCollaborator);
        } else
        if (arraysCollaborator[i + 1] != arraysCollaborator[i]) {
            collArray.push(idCollaborator);
        }
    }
}

function sendCoach() {

    for (let i = 0; i < 5; i++) {
        IdRandom();

        if (arraysCoach[i] == null) {
            coaArray.push(idCoach);
        } else
        if (arraysCoach[i + 1] != arraysCoach[i]) {
            coaArray.push(idCoach);
        }
    }
}


function sendImages() {

    for (let i = 0; i < 5; i++) {
        IdRandom();

        if (arrayImages[i] == null) {
            imgArray.push(idImages);
        } else
        if (arrayImages[i + 1] != arrayImages[i]) {
            imgArray.push(idImages);
        }
    }
}

function sendUser() {
    
    userArry = arraysUser[Math.floor(Math.random() * arraysUser.length)]._id;
}


module.exports = {
    sendColloborator,
    sendUser,
    sendImages,
    sendCoach

}